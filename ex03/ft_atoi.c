/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/30 00:37:01 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/03 14:47:57 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief   Checks if a character is a positive/negative sign.
 * 
 * @param c     Character to check.
 * 
 * @return  '1' if it's a sign; '0' otherwise.
 */
int	is_sign(char c)
{
	return (c == '-' || c == '+');
}

/**
 * @brief   Checks if a character is a number.
 * 
 * @param c     Character to check.
 * 
 * @return  '1' if it's a number; '0' otherwise.
 */
int	is_number(char c)
{
	return ('0' <= c && c <= '9');
}

/**
 * @brief	Checks if a character it's a space or "something strange".
 * 
 * @param c		Character to check.
 * 
 * @return	'1' if "strange"; '0' otherwise;
 */
int	is_strange(const char c)
{
	return (c == '\t'
		|| c == '\n'
		|| c == '\v'
		|| c == '\f'
		|| c == '\r'
		|| c == ' ');
}

int	ft_atoi(char *str)
{
	int	sgn;
	int	num;

	if (!*str)
		return (0);
	while (is_strange(*str))
		str++;
	sgn = 1;
	while (is_sign(*str))
	{
		if (*str == '-')
			sgn = -sgn;
		str++;
	}
	num = 0;
	while (*str && is_number(*str))
	{
		num = num * 10 + *str - '0';
		str++;
	}
	return (sgn * num);
}
