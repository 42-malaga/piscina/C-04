/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/30 00:37:10 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/03 15:20:07 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief   Calculates a step in the process of base change.
 * 
 * @param numb	Number to convert.
 * @param base  Base number value.
 * @param symb	Base symbols.
 */
void	iterate_base(unsigned int numb, unsigned int base, char symb[])
{
	if (base <= numb)
		iterate_base(numb / base, base, symb);
	write(1, &symb[numb % base], 1);
}

/**
 * @brief   Calculates the length of a string.
 *          C 01 - 06.
 * 
 * @param str   String to count.
 * 
 * @return  String's length.
 */
int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

/**
 * @brief   Checks if a base is valid: it has 2 or more elements, does
 *          not contain repeated elements and does not contain '-' or '+'.
 * 
 * @param base  Array of base symbols.
 * 
 * @return  '1' if it's a valid base; '0' otherwise. 
 */
int	is_valid_base(char *base)
{
	int	i;
	int	j;

	if (*base == '\0' || *(base + 1) == '\0')
		return (0);
	i = 0;
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	if (is_valid_base(base))
	{
		if (nbr < 0)
		{
			write(1, "-", 1);
			nbr = -nbr;
		}
		iterate_base(nbr, ft_strlen(base), base);
	}
}
