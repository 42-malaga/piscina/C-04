/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/30 00:37:01 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/03 16:35:40 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a space or "something strange".
 *
 * @param c		Character to check.
 *
 * @return	'1' if "strange"; '0' otherwise;
 */
int	is_strange(const char c)
{
	return (c == '\t'
		|| c == '\n'
		|| c == '\v'
		|| c == '\f'
		|| c == '\r'
		|| c == ' ');
}

/**
 * @brief	Checks if a character belongs to the symbols of a base.
 *
 * @param c		Character to check.
 * @param sym  	Base symbols.
 *
 * @return	String length.
 */
int	in_base(char c, const char *sym)
{
	while (*sym)
	{
		if (c == *sym)
			return (1);
		sym++;
	}
	return (0);
}

/**
 * @brief   Checks if a sym is valid: it has 2 or more elements, does
 *          not contain repeated elements and does not contain '-' or '+'.
 *
 * @param sym  	Base symbols.
 *
 * @return  '1' if it's a valid sym; '0' otherwise.
 */
int	is_valid_base(const char *sym)
{
	int	i;
	int	j;

	if (*sym == '\0' || *(sym + 1) == '\0')
		return (0);
	i = 0;
	while (sym[i])
	{
		if (sym[i] == '+' || sym[i] == '-')
			return (0);
		j = i + 1;
		while (sym[j])
		{
			if (sym[i] == sym[j])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

/**
 * @brief   Checks the number to convert to integer.
 *
 * @param str	String from which to extract the number.
 * @param sym  	Base symbols.
 *
 * @return  The number as integer.
 */
int	check_number_base(const char *str, const char *sym)
{
	int	numb;
	int	base;
	int	i;

	numb = 0;
	base = 0;
	while (sym[base])
		base++;
	while (*str && in_base(*str, sym))
	{
		i = 0;
		while (sym[i])
		{
			if (*str == sym[i])
			{
				numb = numb * base + i;
				break ;
			}
			i++;
		}
		str++;
	}
	return (numb);
}

int	ft_atoi_base(char *str, char *base)
{
	int	sgn;

	if (!*str || !is_valid_base(base))
		return (0);
	while (is_strange(*str))
		str++;
	sgn = 1;
	while (*str == '-' || *str == '+')
	{
		if (*str == '-')
			sgn = -sgn;
		str++;
	}
	return (sgn * check_number_base(str, base));
}
